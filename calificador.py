import tomllib, os, argparse, subprocess


class Grade:

    def __init__(self, key, grades_file, students_list, workdir):
        self.key = key
        self.work = grades_file["work"]
        self.title = self.get_title(grades_file)
        self.author = self.get_author(students_list)
        self.grade = self.get_grade(self.get_scores(grades_file))
        self.comments = self.get_comments(grades_file)
        self.work_filepath = self.get_work_filepath(workdir)
        self.work_exists = os.path.exists(self.work_filepath)
        self.output_filepath = self.get_output_filepath()
        self.is_gradable = (self.author != "Unknown") and (self.work_exists)

    def get_title(self, grades_file):
        return f'{grades_file["work"]} de {grades_file["course"]}'

    def get_author(self, students_list):
        if self.key in students_list:
            return (
                students_list[self.key]["name"]
                + " "
                + students_list[self.key]["lastname"]
            )
        else:
            return "Unknown"

    def get_comments(self, grades_file):
        scores = ["\\pts{" + str(s) + "} " for s in self.get_scores(grades_file)]
        comments = [c for i, c in grades_file[self.key]["comment"].items()]
        return "\n".join(scores[i] + c for i, c in enumerate(comments))

    def get_scores(self, grades_file):
        return [s for i, s in grades_file[self.key]["score"].items()]

    def get_grade(self, scores):
        return str(int(sum(scores) / len(scores)))

    def get_work_filepath(self, workdir):
        filename = "".join([self.work, "_", self.key, ".pdf"]).lower().replace(" ", "_")
        return os.path.join(workdir, filename)

    def get_output_filepath(self):
        return self.work_filepath.replace(".pdf", "_calificada.tex")

    def fill_template(self, template_file):
        filled_template = template_file.replace(":title:", self.title)
        filled_template = filled_template.replace(":author:", self.author)
        filled_template = filled_template.replace(":grade:", self.grade)
        filled_template = filled_template.replace(":comments:", self.comments)
        filled_template = filled_template.replace(":work_filepath:", self.work_filepath)
        return filled_template

    def write_filled_template(self, template_file):
        if self.is_gradable:
            with open(self.output_filepath, "w") as output_file:
                output_file.write(self.fill_template(template_file))

    def compile_filled_template(self):
        if os.path.exists(self.output_filepath):
            subprocess.call(
                f"latexmk -f -pdflua -outdir={os.path.dirname(self.output_filepath)} -quiet {self.output_filepath}",
                shell=True,
                stdout=open(os.devnull, "wb"),
            )

    def __str__(self):
        if self.is_gradable:
            return f"{self.work} by {self.author}. Grade: {self.grade}"
        elif self.author == "Unknown" and self.work_exists:
            return f"Error: {self.key} is unknown"
        elif self.author != "Unknown" and not self.work_exists:
            return f"Error: {self.work_filepath} doesn't exist"
        else:
            return (
                f"Error: {self.key} is unknown and {self.work_filepath} doesn't exist"
            )


def print_banner(grades_file):
    banner = f'Calificando {grades_file["work"]} de {grades_file["course"]}'
    div = "".join("=" for i in range(len(banner)))
    print(banner)
    print(div)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--list", type=str)
    parser.add_argument("-g", "--grades", type=str)
    parser.add_argument("-d", "--workdir", type=str)
    parser.add_argument("-t", "--template", type=str)
    args = parser.parse_args()

    with open(args.list, "rb") as file:
        students_list = tomllib.load(file)

    with open(args.grades, "rb") as file:
        grades_file = tomllib.load(file)

    with open(args.template, "r") as file:
        template_file = "".join(file.readlines())

    print_banner(grades_file)

    grades = [
        Grade(k, grades_file, students_list, args.workdir)
        for k in grades_file
        if k != "work" and k != "course"
    ]

    for grade in grades:
        print(grade)
        grade.write_filled_template(template_file)
        grade.compile_filled_template()
