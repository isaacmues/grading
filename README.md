# Calificaciones

---

Plantillas y scripts para calificar.


## Plantilla en LuaTeX

Esta plantilla usa LuaTeX para crear un archivo que calcula automáticamente la
calificación y anexa el archivo original de la tarea. La plantilla consiste de
dos partes: el preámbulo (`preamble.tex`), que contiene código en Lua y 
algunas macros útiles, y el archivo principal (`template.tex`) donde se
escriben los comentarios. También es necesario un archivo CSV que contenga los
nombres de los estudiantes.

Para que funcione es necesario copiar el archivo `template.tex` usando el 
formato `tn-apellido-apellido-calificacion.tex`, donde `t` puede ser `t` o `e`
para tareas o exámenes respectivamente y `n` es un número desde `01` hasta
`99`. Por ejemplo, la estructura de los archivos para la tarea 1 sería

```
Electromagnetismo
|
+- preamble.tex
|
+- template.tex
|
+- grades/
|  |
|  +- t01-garcia-hernandez-calificacion.tex
|  |
|  +- t01-hernandez-garcia-calificacion.tex
|  |
|  +- ...
|
+- homeworks/
   |
   +- t01-garcia-hernandez.pdf
   |
   +- t01-hernandez-garcia.pdf
   |
   +- ...
```

Para colocar la puntuación de los problemas hay que usar el comando de TeX
`\point{f}` donde `f` es un número entre 0.0 y 1.0 que indica la corrección de
la respuesta. Este comando acepta otro número `w` en caso de querer un promedio
ponderado distinto y se escribe `\point[w]{f}` de tal forma que cuando se
calcule el promedio este ejercicio valdrá `w * f`.

Finalmente, para compilar utilizo el programa `latexmk` y hay que estar dentro
del directorio `grades`. El comando sería el siguiente para la primera tarea:


```
latexmk -pdflua t01-garcia-hernandez-calificacion.tex
```

## Algunas notas sobre Classroom

### Resubir una tarea calificada

Desde una computadora, la forma más sencillas de acceder al Drive de la clase
es usando el ícono que está arriba a la derecha.

![Inicio de Google Classroom](./imagenes/classroom_inicio_anotado.png)

![Drive del Classroom](./imagenes/classroom_drive.png)

Ahora, se hace click derecho sobre el archivo que se desea reemplazar y en el
menú se sigue `Información del archivo > Gestionar versiones`. Se abre una nueva
ventana donde está la opción de subir el archivo.

![Menú de versiones en Drive](./imagenes/gestionar_versiones_classroom_anotado.png)
